import React, { memo } from "react";

export default function CartShoe({
  cart,
  handleDeleteShoe,
  handleChangeQuantity,
}) {
  const renderContentCart = () => {
    return cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img style={{ width: 80 }} src={item.image} />
          </td>
          <td>
            <button
              onClick={() => {
                handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <strong className="mx-3">{item.soLuong}</strong>
            <button
              onClick={() => {
                handleChangeQuantity(item.id, 1);
              }}
              className="btn btn-primary"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                handleDeleteShoe(item.id);
              }}
              className="btn  border-danger text-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
            <th>Quantity</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{renderContentCart()}</tbody>
      </table>
    </div>
  );
}
