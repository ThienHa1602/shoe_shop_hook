import React, { useMemo, useState } from "react";
import CartShoe from "./CartShoe";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import { shoeArr } from "./dataShoe";

export default function Ex_Shoe_Hook() {
  const [shoeDetail, setShoeDetail] = useState(shoeArr[0]);
  const [cart, setCart] = useState([]);
  const handleViewDetail = (item) => {
    setShoeDetail(item);
  };
  const handleAddProduct = (shoe) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }
    setCart(cloneCart);
  };
  const handleDeleteShoe = (id) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    cloneCart.splice(index, 1);
    setCart(cloneCart);
  };
  const handleChangeQuantity = (id, luaChon) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    cloneCart[index].soLuong = cloneCart[index].soLuong + luaChon;
    cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
    setCart(cloneCart);
  };
  return (
    <div>
      <div className="row">
        <div className="col-8">
          <CartShoe
            cart={cart}
            handleDeleteShoe={handleDeleteShoe}
            handleChangeQuantity={handleChangeQuantity}
          />
        </div>
        <div className="col-4">
          <ListShoe
            handleViewDetail={handleViewDetail}
            shoeArr={shoeArr}
            handleAddProduct={handleAddProduct}
          />
        </div>
      </div>
      <DetailShoe shoeDetail={shoeDetail} />
    </div>
  );
}
