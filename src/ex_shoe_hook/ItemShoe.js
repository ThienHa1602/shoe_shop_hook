import React, { memo } from "react";

export default function ItemShoe({
  dataShoe,
  handleViewDetail,
  handleAddProduct,
}) {
  return (
    <div className="col-6">
      <div className="card text-left">
        <img
          style={{ width: "10vw" }}
          className="card-img-top"
          src={dataShoe.image}
        />
        <div className="card-body text-center">
          <h4 className="card-title">{dataShoe.price}</h4>
          <button
            onClick={() => {
              handleViewDetail(dataShoe);
            }}
            className="btn btn-success"
          >
            View Detail
          </button>
          <button
            onClick={() => {
              handleAddProduct(dataShoe);
            }}
            className="btn btn-warning"
          >
            Add
          </button>
        </div>
      </div>
    </div>
  );
}
