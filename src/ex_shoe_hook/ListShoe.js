import React, { memo } from "react";
import ItemShoe from "./ItemShoe";

export default function ListShoe({
  handleViewDetail,
  shoeArr,
  handleAddProduct,
}) {
  const renderListShoe = () => {
    return shoeArr.map((item, index) => {
      return (
        <ItemShoe
          key={index}
          dataShoe={item}
          handleViewDetail={handleViewDetail}
          handleAddProduct={handleAddProduct}
        />
      );
    });
  };
  return <div className="row">{renderListShoe()}</div>;
}
