import React, { memo } from "react";

export default function DetailShoe({ shoeDetail }) {
  let { name, price, description, shortDescription, quantity, image } =
    shoeDetail;
  return (
    <div className="card">
      <img style={{ width: 200 }} className="card-img-top" src={image} />
      <div className="card-body">
        <h4 className="card-title">{name}</h4>
        <p className="card-text">{price}</p>
      </div>
      <ul className="list-group list-group-flush">
        <li className="list-group-item">{quantity}</li>
        <li className="list-group-item">{description} </li>
        <li className="list-group-item">{shortDescription} </li>
      </ul>
    </div>
  );
}
